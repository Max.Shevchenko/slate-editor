import React from 'react';
import {TextEditor} from './components/TextEditorContainer'

const App = () => {
    return (
        <div>
            <TextEditor/>
        </div>
    );
};

export default App;
