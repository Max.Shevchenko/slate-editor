import React, {Component} from 'react'
import {Editor} from 'slate-react'
import {Value} from 'slate'
import BoldMark from "./BoldMark";


const initValue = Value.fromJSON({
    document: {
        nodes: [
            {
                object: 'block',
                type: 'paragraph',
                nodes: [
                    {
                        object: 'text',
                        leaves: [
                            {
                                text: 'My first paragraph!'
                            }
                        ]
                    }
                ]
            }
        ]
    }
})


class TextEditor extends Component {



    state = {
        value: initValue
    }


    handleChange = ({value}) => {
        this.setState({
            value
        })
    }

    handleKeyDown = (e, editor, next) => {

        switch (e.key) {
            case 'b':
                if (e.ctrlKey) {
                    e.preventDefault()
                    editor.toggleMark('bold')
                }
                break
            default:
                return next()
        }

    }


    renderMark = props => {
        switch (props.mark.type) {
            case 'bold':
                return <BoldMark {...props}/>
        }
    }

    render() {
        return (
            <Editor value={this.state.value}
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                    renderMark={this.renderMark}
            />
        )
    }
}

export default TextEditor